import countryWiseData from "../data/countryWise";

// Most commonly used functions on country api

export const years = countryWiseData.aggregations.year.buckets.map(function(array){ return array.key });

export const getYearData = (yearKey) => {
	countryWiseData.aggregations.year.buckets.filter(function(year){return (year.key === yearKey)});
} 

export const getMonthsFromYear = (yearKey) => {
	// const countryWiseData1 = countryWiseData;
	const year = countryWiseData.aggregations.year.buckets.filter(function(year){return (year.key == yearKey)})[0];
	return year ? year.months.buckets.map((month) => month.key ).sort((m1,m2) => m1-m2 ) : null;
}

export const getMonthData = (yearKey, monthKey) => {
	const year = countryWiseData.aggregations.year.buckets.filter((year) => (year.key == yearKey))[0];
	const month = year.months.buckets.filter((month) => (month.key == monthKey) )[0];
	return month ? month : {};
}

export const getStatesDataFromMonthData = (monthData) => {
	return monthData.buckets;
}