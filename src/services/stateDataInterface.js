import stateWiseData from "../data/stateWise";

// Most commonly used functions on the state data api

export const years = stateWiseData.aggregations.year.buckets.map(function(array){ return array.key });

export const getYearData = (yearKey) => {
	stateWiseData.aggregations.year.buckets.filter(function(year){return (year.key === yearKey)});
} 

export const getMonthsFromYear = (yearKey) => {
	const year = stateWiseData.aggregations.year.buckets.filter(function(year){return (year.key == yearKey)})[0];
	return year ? year.month.buckets.map((month) => month.key ).sort() : null;
}

// get the year using year key 
// get the month from that year using month key
// return month data if present or return empty array
export const getMonthData = (yearKey, monthKey) => {
	const year = stateWiseData.aggregations.year.buckets.filter((year) => (year.key == yearKey))[0];
	const month = year.month.buckets.filter((month) => (month.key == monthKey) )[0];
	return month ? month.splitBy.buckets : [];
}

export const getStatesDataFromMonthData = (monthData) => {
	return monthData.buckets;
}


