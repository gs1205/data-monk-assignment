import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import * as countryDataInterface from "./services/countryDataInterface";
import * as stateDataInterface from "./services/stateDataInterface";
import "antd/dist/antd.css";
import { Select, message } from "antd";
import styled from "styled-components";

const getMonthName = {
  1: "January",
  2: "February",
  3: "March",
  4: "April",
  5: "May",
  6: "June",
  7: "July",
  8: "August",
  9: "September",
  10: "October",
  11: "November",
  12: "December"
};

class App extends Component {
  // year and month is for currently selected filter values
  state = {
    years: [],
    year: "",
    months: [],
    month: -1,
    statesData: {},
    countryData: {}
  };

  selectYear = year => {
    if (this.state.year != year) {
      const months = countryDataInterface.getMonthsFromYear(year);
      const statesData = stateDataInterface.getMonthData(year, months[0]);
      const countryData = countryDataInterface.getMonthData(year, months[0]);
      this.setState({
        year: year,
        months: months,
        month: months[0],
        statesData: statesData,
        countryData: countryData
      });
    }
  };

  selectMonth = month => {
    if (this.state.month != month) {
      const statesData = stateDataInterface.getMonthData(
        this.state.year,
        month
      );
      const countryData = countryDataInterface.getMonthData(
        this.state.year,
        month
      );
      this.setState({
        month: month,
        statesData: statesData,
        countryData: countryData
      });
    }
  };

  // Mimicking the behaviour of api call, we want to get the data in didMount and then update the state
  // When a component is mounted, we load years from country api
  // then we load all months using the first value of the year
  // year[0] and month[0] are default values
  // Then we get states data and country data for that month and year
  componentDidMount() {
    const years = countryDataInterface.years;
    const months = countryDataInterface.getMonthsFromYear(years[0]);
    this.setState({
      years: years,
      months: months,
      year: years[0],
      month: months[0],
      statesData: stateDataInterface.getMonthData(years[0], months[0]),
      countryData: countryDataInterface.getMonthData(years[0], months[0])
    });
  }

  getLessThan45States = () => {
    if (this.state.countryData.hmrPercentiles) {
      const countryHmrData = this.state.countryData.hmrPercentiles.values;
      return this.state.statesData.filter(
        state => state.hmrPercentiles.values["50.0"] <= countryHmrData["45.0"]
      );
    } else {
      // message.error("Country data not present to compare");
      return [];
    }
  };

  getMedianStates = () => {
    if (this.state.countryData.hmrPercentiles) {
      const countryHmrData = this.state.countryData.hmrPercentiles.values;
      return this.state.statesData.filter(state => {
        let stateMedianValue = state.hmrPercentiles.values["50.0"];
        return (
          stateMedianValue > countryHmrData["45.0"] &&
          stateMedianValue <= countryHmrData["60.0"]
        );
      });
    } else {
      return [];
    }
  };

  getStatesGreaterThan60 = () => {
    if (this.state.countryData.hmrPercentiles) {
      const countryHmrData = this.state.countryData.hmrPercentiles.values;
      return this.state.statesData.filter(
        state => state.hmrPercentiles.values["50.0"] > countryHmrData["60.0"]
      );
    } else {
      return [];
    }
  };

  // creating the table to display
  createTable = () => {
    const lessThan45States = this.getLessThan45States();
    const medianStates = this.getMedianStates();
    const statesGreaterThan60 = this.getStatesGreaterThan60();
    const length1 = lessThan45States.length,
      length2 = medianStates.length,
      length3 = statesGreaterThan60.length;
    let maxLength = 0,
      i = 0;

    // getting max because that will be the number of rows that we want  
    if (length1 > length2) {
      maxLength = length1 > length3 ? length1 : length3;
    } else {
      maxLength = length2 > length3 ? length2 : length3;
    }
    let table = [],
      children = [];

    for (i = 0; i < maxLength; i++) {
      children = [];
      // if a data is not present, we return empty string
      children.push(
        <td style={{color: "red"}}>{lessThan45States[i] ? lessThan45States[i].key : ""}</td>
      );
      children.push(<td>{medianStates[i] ? medianStates[i].key : ""}</td>);
      children.push(
        <td>{statesGreaterThan60[i] ? statesGreaterThan60[i].key : ""}</td>
      );
      table.push(<tr key={i}>{children}</tr>);
    }
    return table;
  };

  render() {
    const Option = Select.Option
    return (
      <div className="App">
        <h2> Median Hours Machine Run </h2>
        <Filters>
          <span> Filters: </span>
          {this.state.year && (
            <Filter>
              <Select style={{width: "100%"}} value={this.state.year} onChange={this.selectYear}>
                {this.state.years.map(year => (
                  <Option value={year}> {year} </Option>
                ))}
              </Select>
            </Filter>
            )
          }

          { this.state.month && (this.state.month != -1) && (
            <Filter>
              <Select style={{width: "100%"}} value={getMonthName[this.state.month]} onChange={this.selectMonth}>
                {this.state.months.map(month => (
                  <Option value={month}>
                    {getMonthName[month]}
                  </Option>
                ))}
              </Select>
            </Filter>
            )
          }
        </Filters>

        <div>
          <table>
            <thead>
              <th>{"States < 45%"}</th>
              <th>{"States < 60% and > 45%"}</th>

              <th>{"States > 60%"}</th>
            </thead>
            <tbody>{this.state.months && this.createTable()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

const Filters = styled.div`
  padding-top: 1em;
  padding-bottom: 1em;
  width: 80%;
  @media (min-width: 1000px) {
    width: 60%;
  }
  @media (min-width: 1200px) {
    width: 50%;
  }
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 2fr 3fr;
  align-items: center;
`;

const Filter = styled.div`
  width: 80%;
  display: inline-block;

`;




export default App;
